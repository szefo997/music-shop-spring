<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Here is the detail information of the product</h1>
            <p class="lead">Here is the detail information of the product</p>
        </div>

        <div class="container" ng-app="cartApp">
            <div class="row">
                <div class="col-md-5">
                    <td><img src="<c:url value="/resources/images/${product.productId}.png" />" alt="image"
                             style="max-height: 200px; display: block; margin: 0 auto;"></td>
                </div>
                <div class="col-md-5">
                    <h3>${product.productName}</h3>
                    <p>${product.productDescription}</p>
                    <p>
                        <strong>Manufacturer</strong>: ${product.productManufacturer}
                    </p>
                    <p>
                        <strong>Category</strong>: ${product.productCategory}
                    </p>
                    <p>
                        <strong>Condition</strong>: ${product.productCondition}
                    </p>
                    <h4>${product.productPrice} USD</h4>
                    <br>

                    <c:set var="role" scope="page" value="${param.role}"/>
                    <c:set var="url" scope="page" value="/product/productList"/>
                    <c:if test="${role='admin'}">
                        <c:set var="url" scope="page" value="/admin/productInventory"/>
                    </c:if>

                    <p ng-controller="cartCtrl">
                        <a href="<spring:url value="${url}" />" class="btn btn-default">Back</a>
                        <a class="btn btn-warning btn-lg" ng-click="addToCart(${product.productId})">
                            <span class="glyphicon glyphicon-shopping-cart"></span>Order now
                        </a>
                        <a href="<spring:url value="/customer/cart" />" class="btn btn-default">
                            <span class="glyphicon glyphicon-hand-right">View Cart</span>
                        </a>
                    </p>

                </div>
            </div>
        </div>

        <%@include file="template/footer.jsp" %>

