<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="szefo">
    <%--<link rel="icon" href="../../favicon.ico">--%>

    <title>My music store</title>
    <script src="<c:url value='/resources/js/angular.min.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/controller.js' />"></script>
    <link href="<c:url value='/resources/css/bootstrap.min.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/bootstrap-theme.min.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/carousel.css' />" rel="stylesheet">
    <link href="<c:url value='/resources/css/main.css' />" rel="stylesheet">
</head>

<!-- NAVBAR
================================================== -->
<body>
<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">My music store</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<c:url value='/' />">Home</a></li>
                        <li><a href="<c:url value='/product/productList' />">Products</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <c:choose>
                            <c:when test="${pageContext.request.userPrincipal.name == null}">
                                <li><a href="<c:url value='/login' />">Log in</a></li>
                                <li><a href="<c:url value='/register'/>">Register</a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a>Welcome: ${pageContext.request.userPrincipal.name}</a></li>
                                <c:choose>
                                    <c:when test="${pageContext.request.userPrincipal.name != 'admin'}">
                                        <li><a href="<spring:url value='/customer/cart'/>">Cart</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="<spring:url value='/admin'/>">Admin</a></li>
                                    </c:otherwise>
                                </c:choose>
                                <li><a href="<c:url value='/j_spring_security_logout'/>">Log out</a></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>