<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Add product</h1>
            <p class="lead">Fill the below information to add a product:</p>
        </div>

        <form:form action="${pageContext.request.contextPath}/admin/product/addProduct" method="post"
                   commandName="product" enctype="multipart/form-data">

        <div class="form-group">
            <label for="name">Name:</label>
            <form:errors path="productName" cssStyle="color: red" />
            <form:input path="productName" id="name" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="category">Category:</label>
            <label class="checkbox-inline">
                <form:radiobutton path="productCategory" id="category" value="instrument"/>
                Instrument
            </label>
            <label class="checkbox-inline">
                <form:radiobutton path="productCategory" id="category" value="record"/>
                Record
            </label>
            <label class="checkbox-inline">
                <form:radiobutton path="productCategory" id="category" value="accessory"/>
                Accessory
            </label>
        </div>

        <div class="form-group">
            <label for="name">Description:</label>
            <form:textarea path="productDescription" id="description" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="price">Price:</label>
            <form:errors path="productPrice" cssStyle="color: red" />
            <form:input path="productPrice" id="price" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="condition">Condition:</label>
            <label class="checkbox-inline">
                <form:radiobutton path="productCondition" id="condition" value="new"/>
                New
            </label>
            <label class="checkbox-inline">
                <form:radiobutton path="productCondition" id="condition" value="used"/>
                Used
            </label>
        </div>

        <div class="form-group">
            <label for="status">Status:</label>
            <label class="checkbox-inline">
                <form:radiobutton path="productStatus" id="status" value="active"/>
                Active
            </label>
            <label class="checkbox-inline">
                <form:radiobutton path="productStatus" id="status" value="inactive"/>
                Inactive
            </label>
        </div>

        <div class="form-group">
            <label for="unitInStack">Unit in Stock:</label>
            <form:errors path="unitInStack" cssStyle="color: red" />
            <form:input path="unitInStack" id="unitInStock" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer:</label>
            <form:input path="productManufacturer" id="manufacturer" class="form-Control"/>
        </div>

        <div class="form-group">
            <label for="productImage">Upload image:</label>
            <form:input path="productImage" id="productImage" type="file" class="form:input-large"/>
        </div>

        <br><br>

        <input type="submit" value="Submit" class="btn btn-default">
        <a href="<c:url value="/admin/productInventory"/> ">Cancel</a>

        </form:form>


        <%@include file="template/footer.jsp" %>

