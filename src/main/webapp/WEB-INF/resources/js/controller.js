/**
 * Created by szefo on 11.10.16.
 */
var cartApp = angular.module("cartApp", []);
var path = '/music/rest/cart/';

cartApp.controller("cartCtrl", function ($scope, $http) {

    $scope.refreshCart = function () {
        console.log("REFRESHING");
        $http.get(path + $scope.cartId).success(function (data) {
            $scope.cart = data;
        })
    };

    $scope.clearCart = function () {
        console.log("CLEARING");
        console.log($scope.cartId);
        $http.delete(path + $scope.cartId).success(
            function () {
                $scope.refreshCart();
            }).error(function (data) {
            console.log("Error :" + data);
        });
    };

    $scope.initCartId = function (cartId) {
        console.log("INIT");
        $scope.cartId = cartId;
        $scope.refreshCart(cartId);
    };

    $scope.addToCart = function (productId) {
        console.log("ADDING");
        $http.put(path + 'add/' + productId).success(function () {
            console.log("added");
            //alert('Product successfully added to the cart!');
        }).error(function (data) {
            console.log('error ' + data);
        });
    };

    $scope.removeFromCart = function (productId) {
        console.log("REMOVING");
        $http.put(path + 'remove/' + productId).success(function () {
            $scope.refreshCart();
        });
    };

    $scope.calGrandTotal = function () {
        var grandTotal = 0;
        if($scope.cart === undefined){return;}
        for (var i = 0; i < $scope.cart.cartItems.length; i++) {
            grandTotal += $scope.cart.cartItems[i].totalPrice;
        }
        return grandTotal;
    }

});