package com.emusicstore.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by szefo on 15.10.16.
 */
@Entity
public class CustomerOrder implements Serializable {

    private static final long serialVersionUID = -5901607965246195506L;

    @Id
    @GeneratedValue
    private int costumerOrderId;

    @OneToOne
    @JoinColumn(name = "cartId")
    private Cart cart;

    @OneToOne
    @JoinColumn(name = "customerId")
    private Customer customer;

    @OneToOne
    @JoinColumn(name = "billingAddressId")
    private BillingAddress billingAddress;

    @OneToOne
    @JoinColumn(name = "shippingAddressId")
    private ShippingAddress shippingAddress;

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public int getCostumerOrderId() {
        return costumerOrderId;
    }

    public void setCostumerOrderId(int costumerOrderId) {
        this.costumerOrderId = costumerOrderId;
    }

    @Override
    public String toString() {
        return "CustomerOrder{" +
                "costumerOrderId=" + costumerOrderId +
                ", cart=" + cart +
                ", customer=" + customer +
                ", billingAddress=" + billingAddress +
                ", shippingAddress=" + shippingAddress +
                '}';
    }
}
