package com.emusicstore.dao.impl;

import com.emusicstore.dao.CustomerDao;
import com.emusicstore.model.Authority;
import com.emusicstore.model.Cart;
import com.emusicstore.model.Customer;
import com.emusicstore.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by szefo on 16.10.16.
 */
@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void addCustomer(Customer customer) {
        Session session = sessionFactory.getCurrentSession();

//        customer.getBillingAddress().setCustomer(customer);
//        customer.getShippingAddress().setCustomer(customer);

        session.saveOrUpdate(customer);
        session.saveOrUpdate(customer.getBillingAddress());
        session.saveOrUpdate(customer.getShippingAddress());

        User user = new User();
        user.setUsername(customer.getUsername());
        user.setPassword(customer.getPassword());
        user.setCustomerId(customer.getCustomerId());

        Authority auth = new Authority();
        auth.setUsername(customer.getUsername());
        auth.setAuthority("ROLE_USER");

        session.saveOrUpdate(user);
        session.saveOrUpdate(auth);

        Cart cart = new Cart();
//        cart.setCustomer(customer);
        customer.setCart(cart);

        session.save(customer);
        session.save(cart);

        session.flush();

    }

    public Customer getCustomerById(int customerId) {
        Session session = sessionFactory.getCurrentSession();
        return (Customer) session.get(Customer.class, customerId);
    }

    public Customer getCustomerByUserName(String userName) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Customer WHERE username = ?");
        query.setString(0, userName);
        return (Customer) query.uniqueResult();
    }

    public List<Customer> getAllCustomer() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Customer");
        List<Customer> customers = query.list();
        return customers;
    }

}
