package com.emusicstore.dao;

import com.emusicstore.model.CustomerOrder;

/**
 * Created by szefo on 20.11.16.
 */
public interface CustomerOrderDao {

    void addCustomerOrder(CustomerOrder customerOrder);

}
