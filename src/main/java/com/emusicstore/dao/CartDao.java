package com.emusicstore.dao;

import com.emusicstore.model.Cart;

import java.io.IOException;

/**
 * Created by szefo on 10.10.16.
 */
public interface CartDao {

    Cart getCartById(int id);

    Cart validate(int id) throws IOException;

    void update(Cart cart);

}
