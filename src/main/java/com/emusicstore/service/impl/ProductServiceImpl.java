package com.emusicstore.service.impl;

import com.emusicstore.dao.ProductDao;
import com.emusicstore.model.Product;
import com.emusicstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by szefo on 15.10.16.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    public void addProduct(Product product) {
        productDao.addProduct(product);
    }

    public void deleteProduct(int id) {
        productDao.deleteProduct(id);
    }

    public void editProduct(Product product) {
        productDao.editProduct(product);
    }

    public Product getProductById(int id) {
        return productDao.getProductById(id);
    }

    public List<Product> getAllProduct() {
        return productDao.getAllProduct();
    }
}
