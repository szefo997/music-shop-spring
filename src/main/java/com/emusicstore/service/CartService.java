package com.emusicstore.service;

import com.emusicstore.model.Cart;

/**
 * Created by szefo on 20.10.16.
 */
public interface CartService {

    Cart getCartById(int id);

    void update(Cart cart);

}
