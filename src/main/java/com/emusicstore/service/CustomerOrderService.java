package com.emusicstore.service;

import com.emusicstore.model.CustomerOrder;

/**
 * Created by szefo on 20.11.16.
 */
public interface CustomerOrderService {

    void addCustomerOrder(CustomerOrder customerOrder);

    double getCustomerOrderGroundTotal(int cartId);

}
