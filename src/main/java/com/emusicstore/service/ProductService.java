package com.emusicstore.service;

import com.emusicstore.model.Product;

import java.util.List;

/**
 * Created by szefo on 15.10.16.
 */
public interface ProductService {

    void addProduct(Product product);

    void deleteProduct(int id);

    void editProduct(Product product);

    Product getProductById(int id);

    List<Product> getAllProduct();

}
